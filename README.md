# SuperBowlBet - Site web

## Prérequis
Avant de commencer, assurez-vous d'avoir installé les outils suivants :

* Node.js
* npm ou yarn


## Cloner le Référentiel
Clonez le projet depuis le dépôt Git :
```
git clone https://gitlab.com/a.piclin/sbb_web.git
cd sbb_web
```

### Installer les dépendances

```
npm install
# ou
yarn install
```

### Configurer les variables d'environnement
Copiez le fichier **_.env_** en **_.env.local_** et ajustez les paramètres selon votre configuration locale :
```
cp .env .env.local
```

Modifier le fichier **_.env.local_** pour correspondre à votre configuration.

Si vous souhaitez utiliser la version en ligne de sbb_users et sbb_matchs :
```
VITE_URL_API_SBB_USER=https://sbbuser.apic.pro
VITE_URL_API_SBB_MATCH='https://sbbmatch.apic.pro'
```

Si vous souhaitez, utiliser vos versions locales. Exemple :
```
VITE_URL_API_SBB_USER = https://localhost:8000 
VITE_URL_API_SBB_MATCH =  https://localhost:8001
```

Note : Si vous rencontrez des problèmes de connexion avec vos localhosts il est possible que cela soit dû au certificat SSL auto-signé. Aller sur https://localhost:8000 et https://localhost:8001 puis cliquer sur "Paramètres avancés" puis "Continuer vers le site localhost (dangereux)". Laisser les onglets ouverts.

### Exécuter le projet 

```
npm run dev
```
