import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

export default defineConfig({
  plugins: [react()],
  build: {
    rollupOptions: {
      output: {
        manualChunks: undefined,  // Pour éviter les erreurs de fragmentation
      }
    }
  },
  server: {
    watch: {
      usePolling: true,
    },
  },
})