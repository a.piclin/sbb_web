import { createContext } from 'react';

const AppContext = createContext({
  isLoggedIn: false,
  setIsLoggedIn: () => {},
  decodeToken : () => {} ,
  hasRole : () => {},
});

export default AppContext;