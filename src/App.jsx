import './App.css'
import { useState, useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Login from './pages/Login';
import Registration from './pages/Registration';
import Home from './pages/Home';
import Navbar from './Components/Navbar';
import Myspace from './pages/Myspace';
import AllMatchs from './pages/AllMatchs';
import Bets from './pages/Bets';
import AppContext from './contexts/AppContext';
import theme from './theme';
import { ChakraProvider, Box, ColorModeScript } from "@chakra-ui/react";
import ForgetPassword from './pages/ForgetPassword';
import ConfirmEmail from './pages/ConfirmEmail';
import DetailMatch from './pages/DetailMatch';
import Admin from './pages/Admin';
import { jwtDecode } from 'jwt-decode'

function App() {

  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    if (sessionStorage.getItem('jwtToken')) {
      setIsLoggedIn(true);
    }
  }, []);

  const decodeToken = (token) => {
    try {
      return jwtDecode(token)
    } catch (error) {
      console.error('Invalid token:', error)
      return null
    }
  }
  
  const hasRole = (roles, roleToCheck) => {
    return roles.includes(roleToCheck)
  }


  return (
    <ChakraProvider theme={theme}>
    <ColorModeScript initialColorMode={theme.config.initialColorMode} />
    <AppContext.Provider
    value={{
      isLoggedIn,
      setIsLoggedIn: setIsLoggedIn,
      decodeToken : decodeToken,
      hasRole : hasRole
    }}
  >
    <Router>
    <Box bg={"primary"} minH={'100vh'}>
      <Navbar/>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/allmatchs" element={<AllMatchs />} />
        <Route path="/details/:id" element={<DetailMatch />} />
        <Route path="/bets" element={<Bets />} />
        <Route path="/forgetPassword/:resetToken" element={<ForgetPassword />} />
        <Route path="/confirmSuccess" element={<ConfirmEmail isSuccess={"true"} />} />
        <Route path="/failedConfirm" element={<ConfirmEmail />} />


        {isLoggedIn && hasRole(decodeToken(sessionStorage.getItem("jwtToken")).roles, 'ROLE_ADMIN') && 
        <Route path="/admin" element={<Admin />} />
        }

        {isLoggedIn &&
        <Route path="/myspace" element={<Myspace />} />
        }

        {!isLoggedIn &&
        <>
        <Route path="/login" element={<Login />} />
        <Route path="/registration" element={<Registration />} />
        </>
        }
      </Routes>
    </Box>
  </Router>
  </AppContext.Provider>
  </ChakraProvider>
  )
}

export default App
