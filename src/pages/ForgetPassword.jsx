import { useParams,  useNavigate } from "react-router-dom";
import "../css/login.css";
import { useState } from "react";
import axios from "axios";
import {
  InputGroup,
  Input,
  IconButton,
  InputRightElement,
  useToast,
  Button,
  Image,
  Box,
  Card,
  CardBody,
  CardFooter,
  Stack,
  Heading,
  FormControl,
  FormLabel,
  FormHelperText
} from "@chakra-ui/react";
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";

const ForgetPassword = () => {
  const { resetToken } = useParams();
  const [newPassword, setNewPassword] = useState("");
  const [secure, setSecure] = useState(true);
  const toast = useToast();
  const navigate = useNavigate();
  const apiUrl = import.meta.env.VITE_URL_API_SBB_USER;

  const handleResetPassword = async () => {
    try {
      const response = await axios.post(
        `${apiUrl}/api/resetPassword`,
        { 'password' : newPassword, resetToken }
      );
      toast({
        title: "Modification du mot de passe réussie.",
        description: "Essayer de vous connecter.",
        status: "success",
        duration: 9000,
        isClosable: true,
      });

      navigate("/login");
    } catch (error) {
      {
        toast({
          title: "Echec de la modification du mot de passe",
          description: "Veuillez réessayer svp.",
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      }
    }
  };

  const handleSecure = () => {
    setSecure(!secure);
  };

  return (
    <Box p={5}>
      <Card
        direction={{ base: "column", md: "row" }}
        width={{ base: "100%", md: "80%", lg: "60%" }}
        maxWidth={"1200px"}
        className="loginCard"
      >
        <Stack width="100%">
          <CardBody>
            <Heading size="lg" mb={10}>
              Changer de mot de passe
            </Heading>

            <FormControl isRequired mb="15px">
              <FormLabel>Mot de passe</FormLabel>
              <InputGroup>
                <Input
                  type={secure ? "password" : "text"}
                  placeholder="Mot de passe"
                  value={newPassword}
                  onChange={(event) => {
                    setNewPassword(event.target.value);
                  }}
                />
                <InputRightElement>
                  {secure ? (
                    <IconButton
                      bgColor="secondary"
                      color="dark"
                      aria-label="Afficher"
                      icon={<ViewOffIcon />}
                      onClick={handleSecure}
                    />
                  ) : (
                    <IconButton
                      bgColor="secondary"
                      color="dark"
                      aria-label="Masquer"
                      icon={<ViewIcon />}
                      onClick={handleSecure}
                    />
                  )}
                </InputRightElement>
              </InputGroup>
                <FormHelperText>8 caractères minimum.</FormHelperText>
            </FormControl>
          </CardBody>

          <CardFooter flexDirection={"column"}>
            <Button onClick={handleResetPassword} mb={2} bg={"secondary"}>
              Valider
            </Button>
          </CardFooter>
        </Stack>
        <Image
          objectFit="contain"
          src="../logo.png"
          alt="Logo SBB"
          width="100%"
          maxWidth={"500px"}
        />
      </Card>
    </Box>
  );
};

export default ForgetPassword;
