import axios from "axios";
import { useState, useEffect, useContext } from "react";
import {
  Box,
  Heading,
  Text,
  useToast,
  Spinner,
  Center,
  useDisclosure,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  Badge,
} from "@chakra-ui/react";
import ListMatch from "../Components/ListMatch";
import AppContext from "../contexts/AppContext";
import MultipleBetModalContent from "../Components/MultipleBetModalContent";
import Pagination from "../Components/Pagination";
import { LuAlertCircle } from "react-icons/lu";

const Bets = () => {
  const { isLoggedIn, decodeToken } = useContext(AppContext);
  const [isLoading, setIsLoading] = useState(true);
  const [hasError, setHasError] = useState(false);
  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [listMatchs, setListMatchs] = useState([]);
  const [listSelect, setListSelect] = useState(new Map());
  const [bets, setBets] = useState([]);
  const [page, setPage] = useState(1);
  const [totalItems, setTotalItems] = useState(0);
  const itemsPerPage = 10;
  const [myBets, setMyBets] = useState([]);
  const apiUrl = import.meta.env.VITE_URL_API_SBB_MATCH;

  const getMatchs = async () => {
    try {
      const response = await axios.get(
        `${apiUrl}/api/get_all_matchs_to_become?page=${page}`
      );
      setTotalItems(response.data["hydra:totalItems"]);
      return response.data["hydra:member"];
    } catch (error) {
      {
        toast({
          title: "Erreur.",
          description: "Une erreur est survenue.",
          status: "error",
          duration: 9000,
          isClosable: true,
        });
        setHasError(true);
      }
    }
  };

  const getMyBetsNotFinished = async () => {
    try {
      const uuid = decodeToken(sessionStorage.getItem("jwtToken")).uuid;
      const response = await axios.get(
        `${apiUrl}/api/get_all_my_bets_not_finished/${uuid}`,
        {
          headers: {
            Authorization: `Bearer ${sessionStorage.getItem("jwtToken")}`,
          },
        }
      );
      setMyBets(response.data["hydra:member"]);
      return response.data["hydra:member"];
    } catch (error) {
      {
        toast({
          title: "Erreur.",
          description: "Une erreur est survenue.",
          status: "error",
          duration: 9000,
          isClosable: true,
        });
        setHasError(true);
      }
    }
  };

  const getDataPageBets = async () => {
    let listbets;
    const now = new Date();
    setHasError(false);

    if (sessionStorage.getItem("jwtToken")) {
      listbets = await getMyBetsNotFinished();
    }

    let listMatch = await getMatchs();

    const filteredMatches = listMatch.filter((match) => {
      const isMatchNotStarted = now < new Date(match.start_date);
      const isMatchNotInBetsNotFinished = !listbets.some(
        (bet) => bet.id_match === `/api/match/${match.id}`
      );
      return isMatchNotStarted && isMatchNotInBetsNotFinished;
    });

    setListMatchs(filteredMatches);
  };

  useEffect(() => {
    getDataPageBets();
    setIsLoading(false);
  }, [page]);

  const saveAllBet = async () => {
    const uuid = decodeToken(sessionStorage.getItem("jwtToken")).uuid;
    try {
      const response = await axios.post(
        `${apiUrl}/api/add_multiple_bet`,
        {
          uuid_user: uuid,
          multiple_bets: bets,
        }
      );

      toast({
        title: "Paris placés avec succès.",
        description: "Retrouver vos paris dans votre espace personnel.",
        status: "success",
        duration: 9000,
        isClosable: true,
      });
      onClose();
      navigate("/allmatchs");
    } catch (error) {
      {
        toast({
          title: "Erreur.",
          description: "Une erreur est survenue.",
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      }
    }
    onClose();
  };

  const totalPages = Math.ceil(totalItems / itemsPerPage);

  if (isLoading) {
    return (
      <Box display="flex" justifyContent="center" mt={10}>
        <Spinner
          thickness="10px"
          speed="0.80s"
          emptyColor="white"
          color="primary"
          size="xl"
        />
      </Box>
    );
  }

  return (
    <>
      <Box width={{ base: "90%", md: "50%" }} margin={"auto"} pb={20}>
        <Heading as="h2" my={6} className="centerText">
          Sélectionner plusieurs matchs
        </Heading>
        <Center>
          <Text>
            {" "}
            <Badge>Matchs sélectionnés : </Badge> {listSelect.size}
          </Text>
        </Center>
        <Center>
          {isLoggedIn ? (
            <Button 
            onClick={onOpen} 
            isDisabled={listSelect.size <= 0} 
            my={5}
            bg="secondary"
            color="white"
            minWidth="200px"
            padding={10}
            mb={10}
            borderWidth={3}
            style={{boxShadow: '4px 8px 18px #F4AAB9'}}
            _hover={{ bg: 'logo',  color:'black' }}
            transition='all 0.2s cubic-bezier(.08,.52,.52,1)'
            >
              Miser
            </Button>
          ) : (
            <Box display={"flex"} alignItems={"baseline"}>
              <LuAlertCircle />
              <Text mb={10} ms={2}>
                Vous devez avoir un compte pour effectuer un pari.
              </Text>
            </Box>
          )}
        </Center>
        {hasError ? (
          <Text>No data</Text>
        ) : (
          <ListMatch
            listMatchs={listMatchs}
            listSelect={listSelect}
            setListSelect={setListSelect}
          />
        )}
        <Center mt={4}>
          <Pagination page={page} setPage={setPage} totalPages={totalPages} />
        </Center>
      </Box>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader fontFamily={"heading"}>Votre sélection</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <MultipleBetModalContent
              listSelect={listSelect}
              bets={bets}
              setBets={setBets}
            />
          </ModalBody>
          <ModalFooter justifyContent={"space-around"}>
            <Button colorScheme="teal" mr={3} onClick={onClose}>
              Annuler
            </Button>
            <Button bg={"secondary"} onClick={saveAllBet}>
              Valider les paris
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default Bets;
