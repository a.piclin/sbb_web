import "../css/registration.css";
import { useState } from "react";
import axios from "axios";
import {
  Card,
  CardBody,
  CardFooter,
  InputGroup,
  Input,
  IconButton,
  InputRightElement,
  useToast,
  Button,
  Image,
  Box,
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormHelperText,
  Stack,
  Heading,
} from "@chakra-ui/react";
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import { useNavigate, Link } from "react-router-dom";

const Registration = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [lastname, setLastname] = useState("");
  const [firstname, setFirstname] = useState("");
  const toast = useToast();
  const navigate = useNavigate();
  const [secure, setSecure] = useState(false);
  const [isErrorEmail, setIsErrorEmail] = useState(false);
  const [isErrorPassword, setIsErrorPassword] = useState(false);
  const [isErrorFirstname, setIsErrorFirstname] = useState(false);
  const [isErrorLastname, setIsErrorLastname] = useState(false);
  const apiUrl = import.meta.env.VITE_URL_API_SBB_USER;
  const [isTryRegistration, setIsTryRegistration] = useState(false);

  const handleRegistration = () => {
    setIsErrorEmail(false);
    setIsErrorPassword(false);
    setIsErrorFirstname(false);
    setIsErrorLastname(false);

    let hasError = false;

    if (password.length < 8) {
      setIsErrorPassword(true);
      hasError = true;
    }

    if (!/\S+@\S+\.\S+/.test(email)) {
      setIsErrorEmail(true);
      hasError = true;
    }

    if (password.length < 8) {
      setIsErrorPassword(true);
      hasError = true;
    }

    if (lastname.length === 0) {
      setIsErrorLastname(true);
      hasError = true;
    }

    if (firstname.length === 0) {
      setIsErrorFirstname(true);
      hasError = true;
    }

    if (!hasError) {
      sendData();
    }
  };

  const handleSecure = () => {
    setSecure(!secure);
  };

  const sendData = async () => {
    setIsTryRegistration(true)
    try {
      const response = await axios.post(
         `${apiUrl}/api/registration`,
        { email, password, lastname, firstname }
      );
      toast({
        title: "Inscription réussie.",
        description:
          "Votre compte a été créé. Vous devez vérifier votre adresse mail avant de vous connecter.",
        status: "success",
        duration: 9000,
        isClosable: true,
      });
      navigate("/login");
    } catch (error) {
      {
        toast({
          title: "Erreur.",
          description: "Une erreur est survenue.",
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      }
    }
    setIsTryRegistration(false)
  };

  return (
    <Box p={5}>
      <Card
        direction={{ base: "column", md: "row" }}
        width={{ base: "100%", md: "80%", lg: "60%" }}
        maxWidth={"1200px"}
        className="registrationCard"
      >
        <Stack width="100%">
          <CardBody>
            <Heading size="lg" mb={5}>
              S'inscrire
            </Heading>

            <FormControl isInvalid={isErrorFirstname} isRequired mb="15px">
              <FormLabel>Prénom</FormLabel>
              <Input
                type="text"
                value={firstname}
                onChange={(event) => {
                  setFirstname(event.target.value);
                  setIsErrorFirstname(false);
                }}
              />
              {isErrorFirstname && (
                <FormErrorMessage>Champ requis</FormErrorMessage>
              )}
            </FormControl>
            <FormControl isInvalid={isErrorLastname} isRequired mb="15px">
              <FormLabel>Nom</FormLabel>
              <Input
                type="text"
                value={lastname}
                onChange={(event) => {
                  setLastname(event.target.value);
                  setIsErrorLastname(false);
                }}
              />
              {isErrorLastname && (
                <FormErrorMessage>Champ requis.</FormErrorMessage>
              )}
            </FormControl>
            <FormControl isInvalid={isErrorEmail} isRequired mb="15px">
              <FormLabel>Email</FormLabel>
              <Input
                type="text"
                value={email}
                onChange={(event) => {
                  setEmail(event.target.value);
                  setIsErrorEmail(false);
                }}
              />
              {isErrorEmail && (
                <FormErrorMessage>Adresse email invalide</FormErrorMessage>
              )}
            </FormControl>
            <FormControl isInvalid={isErrorPassword} isRequired mb="15px">
              <FormLabel>Mot de passe</FormLabel>
              <InputGroup>
                <Input
                  type={secure ? "password" : "text"}
                  placeholder="Mot de passe"
                  value={password}
                  onChange={(event) => {
                    setPassword(event.target.value);
                    setIsErrorPassword(false);
                  }}
                />
                <InputRightElement>
                  {secure ? (
                    <IconButton
                      bgColor="secondary"
                      color="dark"
                      aria-label="Afficher"
                      icon={<ViewOffIcon />}
                      onClick={handleSecure}
                    />
                  ) : (
                    <IconButton
                      bgColor="secondary"
                      color="dark"
                      aria-label="Masquer"
                      icon={<ViewIcon />}
                      onClick={handleSecure}
                    />
                  )}
                </InputRightElement>
              </InputGroup>
              {!isErrorPassword ? (
                <FormHelperText>8 caractères minimum.</FormHelperText>
              ) : (
                <FormErrorMessage>Mot de passe trop court !</FormErrorMessage>
              )}
            </FormControl>
          </CardBody>

          <CardFooter flexDirection={"column"}>
            <Button onClick={handleRegistration} isLoading={isTryRegistration} mb={2} bg={"secondary"}>S'inscrire</Button>
            <Button
                  variant="link"
                  onClick={() => {
                    navigate("/login");
                  }}
                >
                  Déjà inscrit ?
                </Button>
          </CardFooter>
        </Stack>
        <Image
          objectFit="contain"
          src="./logo.png"
          alt="Logo SBB"
          width="100%"
          maxWidth={'500px'}
          borderRadius={{ base: 0, md: '10%' }}
          pr={{ base: 0, md: 10 }}
        />
      </Card>
    </Box>
  );
};

export default Registration;
