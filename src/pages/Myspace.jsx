import { useState, useEffect, useContext } from "react";
import {
  Tabs,
  TabList,
  TabPanels,
  Tab,
  TabPanel,
  Box,
  Heading,
  useBreakpointValue,
  Text,
  useToast
} from "@chakra-ui/react";
import { MdAccountCircle, MdHistory } from "react-icons/md";
import { IoBarChartSharp } from "react-icons/io5";
import UserInfoCard from "../Components/UserInfoCard";
import HistoryCard from "../Components/HistoryCard";
import ChartsCard from "../Components/ChartsCard";
import AppContext from "../contexts/AppContext";
import axios from "axios";

const Myspace = () => {

  const isBase = useBreakpointValue({ base: true, md: false });
  const toast = useToast();
  const {decodeToken} = useContext(AppContext);
  const apiUrlUser = import.meta.env.VITE_URL_API_SBB_USER;
  const apiUrlMatch = import.meta.env.VITE_URL_API_SBB_MATCH;


  const [dataBets, setDataBets] = useState([
    {
      "calculatedEarnings": 0,
      "end_date": {
        "date": new Date
      }
    }
  ])

  const [historyDataBets, setHistoryDataBets] = useState([]);

  const [infos, setInfos] = useState([]);

  
  const getInfo = async () => {
    const uuid = decodeToken(sessionStorage.getItem("jwtToken")).uuid 
    try {
      const response = await axios.get(`${apiUrlUser}/api/user/${uuid}`, {
        headers: {
          Authorization: `Bearer ${sessionStorage.getItem("jwtToken")}`,
        },
      })
      setInfos(response.data)
    } catch (error) {
      {
        toast({
          title: 'Erreur.',
          description: 'Une erreur est survenue.',
          status: 'error',
          duration: 9000,
          isClosable: true
        })
      }
    }
  }

  const getDataBets = async () => {
    const uuid = decodeToken(sessionStorage.getItem("jwtToken")).uuid 
    try {
      const response = await axios.get(`${apiUrlMatch}/api/get_all_my_bets/${uuid}`, {
        headers: {
          Authorization: `Bearer ${sessionStorage.getItem("jwtToken")}`,
        },
      })
      if(response.data.length > 0) {
        setDataBets(response.data)
        setHistoryDataBets(response.data)
    } 
    } catch (error) {
      {
        toast({
          title: 'Erreur.',
          description: 'Une erreur est survenue.',
          status: 'error',
          duration: 9000,
          isClosable: true
        })
      }
    }
  }

  useEffect(() => {
    getDataBets()
    getInfo()
  }, [])

  return (
    <Box width={{ base: "90%", md: "80%" }} margin={"auto"} pb={20}>
      <Heading as="h2" my={6} className="centerText">
        Mon espace
      </Heading>
      <Tabs isLazy isFitted defaultIndex={1} variant='enclosed' size={'lg'}>
        <TabList>
          <Tab color={'black'} bg={'white'} borderColor={'black'} borderWidth={2} _selected={{ bg: 'secondary' }}>{isBase ? <MdAccountCircle/> : <><MdAccountCircle/><Text ms={2}>Mes informations</Text></>}</Tab>
          <Tab color={'black'} bg={'white'} borderColor={'black'} borderWidth={2} _selected={{ bg: 'secondary' }}>{isBase ? <IoBarChartSharp /> : <><IoBarChartSharp /><Text ms={2}>Dashboard</Text></>}</Tab>
          <Tab color={'black'} bg={'white'} borderColor={'black'} borderWidth={2} _selected={{ bg: 'secondary' }}>{isBase ? <MdHistory /> : <><MdHistory /><Text ms={2}>Historiques</Text></>}</Tab>
        </TabList>
        <TabPanels>
          <TabPanel>
            <UserInfoCard data={infos}/>
          </TabPanel>
          <TabPanel>
            <ChartsCard data={dataBets}/>
          </TabPanel>
          <TabPanel>
            <HistoryCard data={historyDataBets}/>
          </TabPanel>
        </TabPanels>
      </Tabs>
    </Box>
  );
};

export default Myspace;
