import "../css/login.css";
import { useContext, useState, ActivityIndicator } from "react";
import axios from "axios";
import {
  InputGroup,
  Input,
  IconButton,
  InputRightElement,
  useToast,
  Button,
  Image,
  Box,
  Card,
  CardBody,
  CardFooter,
  Stack,
  Heading,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Text
} from "@chakra-ui/react";
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import AppContext from "../contexts/AppContext";
import { useNavigate } from "react-router-dom";
import { FaComputer } from "react-icons/fa6";


const Login = () => {

  const toast = useToast();
  const navigate = useNavigate();
  const { setIsLoggedIn, decodeToken, hasRole } = useContext(AppContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailForget, setEmailForget] = useState("");
  const [secure, setSecure] = useState(true);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const apiUrl = import.meta.env.VITE_URL_API_SBB_USER;
  const [isTryConnect, setIsTryConnect] = useState(false);
 
  const handleLogin = async () => {
    setIsTryConnect(true)
    try {
      const response = await axios.post(
        `${apiUrl}/api/login_check`,
        { email, password }
      );
      const data = response.data;
      const usertoken = data.token;

      if (hasRole(decodeToken(usertoken).roles, 'ROLE_USER') ) {
      sessionStorage.setItem("jwtToken", usertoken);
      toast({
        title: "Connexion réussie.",
        description: "Bienvenue.",
        status: "success",
        duration: 9000,
        isClosable: true,
      });
      setIsLoggedIn(true);
      hasRole(decodeToken(usertoken).roles, 'ROLE_ADMIN') ? navigate("/admin") : navigate("/");
      }
    } catch (error) {
        toast({
          title: "Echec de connexion.",
          description: "Veuillez vérifier vos identifiants.",
          status: "error",
          duration: 9000,
          isClosable: true,
        });
    }
    setIsTryConnect(false)
  };

  const handleSecure = () => {
    setSecure(!secure);
  };

  const handleForgetPassword = async () => {
    try {
      const response = await axios.get(
         `${apiUrl}/api/getEmailResetPassword/${emailForget}`,
      );
      toast({
        title: "Demande effectué avec succès.",
        description: "Vérifiez vos email.",
        status: "success",
        duration: 9000,
        isClosable: true,
      });
      onClose()
    } catch (error) {
      {
        toast({
          title: "Demande impossible.",
          description: "Vérifier votre adresse mail.",
          status: "error",
          duration: 9000,
          isClosable: true,
        });
        onClose()
      }
    }
  }

  return (
    <Box p={5}>
      <Card
        direction={{ base: "column", md: "row" }}
        width={{ base: "100%", md: "80%", lg: "60%" }}
        maxWidth={"1200px"}
        className="loginCard"
      >
        <Stack width="100%">
          <CardBody>
            <Heading size="lg" mb={10}>
              Se connecter
            </Heading>

            <Input
              type="text"
              placeholder="Email"
              value={email}
              onChange={(event) => setEmail(event.target.value)}
              mb={5}
            />
            <InputGroup >
              <Input
                type={secure ? "password" : "text"}
                placeholder="Mot de passe"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
              />
              <InputRightElement>
                {secure ? (
                  <IconButton
                    bgColor="secondary"
                    color="dark"
                    aria-label="Afficher"
                    icon={<ViewOffIcon />}
                    onClick={handleSecure}
                  />
                ) : (
                  <IconButton
                    colorScheme="blue"
                    aria-label="Masquer"
                    icon={<ViewIcon />}
                    onClick={handleSecure}
                  />
                )}
              </InputRightElement>
            </InputGroup>
          </CardBody>

          <CardFooter flexDirection={"column"}>
            <Button onClick={handleLogin} isLoading={isTryConnect} mb={2} bg={"secondary"}>
              Connexion
            </Button>
            <Button
                  mb={2}
                  variant="link"
                  onClick={() => {
                    navigate("/registration");
                  }}
                >
                  Pas encore inscrit ?
            </Button>
            <Button
                  variant="link"
                  onClick={onOpen}
                >
                Mot de passe oublié ?
            </Button>
          </CardFooter>
        </Stack>
        <Image
          objectFit="contain"
          src="./logo.png"
          alt="Logo SBB"
          width="100%"
          maxWidth={"500px"}
        />
      </Card>

      <Box className="appliBureau"  width={{ base: "100%", md: "80%", lg: "60%" }}
        maxWidth={"1200px"} borderRadius={10}>
      <Text mt={4} className="centerText">
            Si vous êtes commentateur, utilisez notre application de bureau. <br></br>
            <a href="https://superbowlbet.apic.pro/SuberBowlBet_Desktop.rar" download className="download">
            Télécharger
            </a>
          </Text>
          <FaComputer size={40} className="iconeCenter"/>
      </Box>


      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Demande rénitialisation du mot de passe</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
              <Input
                type="email"
                value={emailForget}
                onChange={(event) => setEmailForget(event.target.value)}
                placeholder="Entrez votre email"
              />
          </ModalBody>
          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={handleForgetPassword}>
              Valider
            </Button>
            <Button variant="ghost" onClick={onClose}>Annuler</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>


    </Box>
  );
};

export default Login;
