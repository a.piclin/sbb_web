import "../css/home.css";
import React from "react";
import {
  Box,
  Heading,
  Text,
  Image,
  Grid,
  GridItem,
  Stack,
} from "@chakra-ui/react";
import { FaFootballBall } from "react-icons/fa";
import { GiSmartphone } from "react-icons/gi";

import ListMatchOfTheDay from "../Components/ListMatchOfTheDay";

const Home = () => {
  return (
    <Grid
      templateColumns={{
        base: "repeat(1, 1fr)",
        md: "repeat(7, 1fr)",
      }}
      p={4}
      gap={3}
    >
      <GridItem colSpan={{ base: 1, md: 7 }} bg={"secondaryTeam"}>
        <Box className="boxHomeLogo">
          <Image
            src="./logo.png"
            alt="Logo SuperBowlBet"
            className="homeLogo"
            mt={3}
          />
        </Box>
        <Box>
          <Text
            fontFamily={"heading"}
            color={"teamTwo"}
            fontSize={{ base: "28px", md: "35px" }}
            mb={4}
            className="centerText"
          >
            Bienvenue chers amateurs de football !
          </Text>
        </Box>
      </GridItem>
      <GridItem
        colSpan={{ base: 1, md: 4 }}
        bg={"logo"}
        className="homeDescription"
        mb={5}
      >
        <Box className="homePlayer"></Box>
        <Box className="homeText">
          <FaFootballBall size={24} className="iconeCenter"/>
          <Text mt={4} className="centerText">
            SuperBowlBet est dédié à l'événement le plus attendu de l'année, le
            Super Bowl, ainsi qu'à toute la saison de football américain !
          </Text>

          <FaFootballBall size={24} className="iconeCenter"/>
          <Text mt={4} className="centerText">
            Avec SuperBowlBet, vous pouvez facilement accéder aux informations
            des matchs, parier sur vos équipes favorites et obtenir des mises à
            jour instantanées.<br></br>
          </Text>

          <FaFootballBall size={24} className="iconeCenter"/>
          <Text mt={4} className="centerText">
            Que vous soyez un fan passionné ou un parieur avisé, nous avons tout
            ce qu'il vous faut.<br></br>
            Rejoignez-nous pour vivre chaque moment palpitant du football
            américain !
          </Text>

          <Text mt={4} className="centerText">
            Suivez vos paris sur votre smartphone ! <br></br>
            <a href={'/sbb_app.apk'} download="sbb_app.apk" className="download">
            Télécharger l'APK
            </a>
          </Text>
          <GiSmartphone size={40} className="iconeCenter"/>
        </Box>
      </GridItem>
      <GridItem
        rowSpan={3}
        colSpan={{ base: 1, md: 3 }}
        p={5}
        className="homeMatchs"
        bg={"secondary"}
      >
        <ListMatchOfTheDay />
      </GridItem>
      <GridItem colSpan={{ base: 1, md: 4 }} p={12} className="homePartners">
        <Heading as="h2" mb={4}>
          Nos partenaires :
        </Heading>
        <Stack
          direction={{ base: "column", md: "row" }}
          spacing={4}
          align="center"
          justify="center"
        >
          <Image src="./nfl.png" alt="Logo NFL" boxSize="200px" />
          <Image src="./superbowl.png" alt="Logo NFL" boxSize="200px" />
        </Stack>
      </GridItem>
    </Grid>
  );
};

export default Home;
