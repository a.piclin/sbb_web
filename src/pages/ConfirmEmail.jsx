import { Card, CardBody, Text, Heading, Box } from "@chakra-ui/react";

const ConfirmEmail = ({ isSuccess = false }) => {
  return (
    <Box p={30}>
    <Card p={10}>
      <CardBody>
        {isSuccess ? (
          <>
            <Heading as="h2" mb={3}>Email confirmé avec succès !</Heading>
            <Text>Merci pour votre confirmation.</Text>
          </>
        ) : (
          <>
            <Heading as="h2" mb={3}>Échec de la confirmation de l'email</Heading>
            <Text>Le lien de confirmation est invalide.</Text>
          </>
        )}
      </CardBody>
    </Card>
    </Box>
  );
};

export default ConfirmEmail;
