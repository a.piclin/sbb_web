import { useContext, useEffect, useState } from "react";
import {
  Button,
  Box,
  Spinner,
  Center,
  useDisclosure,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useToast,
  Text,
} from "@chakra-ui/react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import MatchCard from "../Components/MatchCard.jsx";
import BetModalContent from "../Components/BetModalContent.jsx";
import AppContext from "../contexts/AppContext.jsx";
import { LuAlertCircle } from "react-icons/lu";

const DetailMatch = () => {
  const { id } = useParams();
  const toast = useToast();
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(true);
  const [match, setMatch] = useState(null);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [choiceBet, setChoiceBet] = useState("Victoire domicile");
  const [amount, setAmount] = useState(0.5);
  const { decodeToken, isLoggedIn } = useContext(AppContext);
  const [listComments, setListComments] = useState([]);
  const [statusMatch, setStatusMatch] = useState("");
  const [myBet, setMyBet] = useState([]);
  const apiUrl = import.meta.env.VITE_URL_API_SBB_MATCH;

  const getStatus = (status, start_date) => {
    if (status === "A venir") {
      const now = new Date();
      if (now > new Date(start_date)) {
        return "En cours";
      } else {
        return status;
      }
    } else {
      return status;
    }
  };

  const getDataMatch = async () => {
    try {
      const response = await axios.get(
        `${apiUrl}/api/match/${id}`
      );

      setMatch(response.data);
      setStatusMatch(getStatus(response.data.status, response.data.start_date));
      return response.data;
    } catch (error) {
      {
        toast({
          title: "Erreur.",
          description: "Une erreur est survenue.",
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      }
    }
  };

  const getCommentsOfMatch = async (id) => {
    try {
      const response = await axios.get(
        `${apiUrl}/api/get_comments_of_match/${id}`
      );
      setListComments(response.data.comments);
    } catch (error) {
      {
        toast({
          title: "Erreur.",
          description: "Une erreur est survenue.",
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      }
    }
  };

  const getMyBet = async (id) => {
    if (sessionStorage.getItem("jwtToken")) {
      const uuid = decodeToken(sessionStorage.getItem("jwtToken")).uuid;
      try {
        const response = await axios.get(
          `${apiUrl}/api/get_my_bet_of_match/${uuid}/${id}`, {
            headers: {
              Authorization: `Bearer ${sessionStorage.getItem("jwtToken")}`,
            },
          }
        );

        if(response.data.length === 1) {
        setMyBet(response.data[0]);
        setChoiceBet(response.data[0].bet);
        setAmount(response.data[0].amount);
        }
      } catch (error) {
        console.error(error)
      }
    }
  };

  const getDetailMatch = async () => {
    setIsLoading(true);
    let response = await getDataMatch();
    let id = response.id;

    if (id) {
      getCommentsOfMatch(id);
    }
    getMyBet(id);
    setIsLoading(false);
  };

  const getRefreshDetailMatch = async () => {
    let response = await getDataMatch();
    let id = response.id;

    if (id) {
      getCommentsOfMatch(id);
    }
    getMyBet(id);
  };

  useEffect(() => {
    getDetailMatch();

    const intervalId = setInterval(() => {
      getRefreshDetailMatch();
    }, 30000);
    return () => clearInterval(intervalId);
  }, []);

  const getLastSegment = (url) => {
    const segments = url.split('/');
    return segments.pop();
  };

  const saveBet = async () => {
    if (match.status === "A venir" && statusMatch === "A venir") {
      const uuid = decodeToken(sessionStorage.getItem("jwtToken")).uuid;
      try {
        let url;
        let body;
        let method;
        let headers;
        if (myBet.id) {
          url = `${apiUrl}/api/update_bet/${getLastSegment(myBet.id)}`;
          body = {
            amount,
            bet: choiceBet,
          };
          method = "patch";
          headers = {
            "Content-Type": "application/merge-patch+json",
            Authorization: `Bearer ${sessionStorage.getItem("jwtToken")}`,
          };
        } else {
          url = `${apiUrl}/api/add_bet`;
          body = {
            uuid_user: uuid,
            id_match: `api/match/${match.id}`,
            amount,
            bet: choiceBet,
          };
          method = "post";
          headers = {
            Authorization: `Bearer ${sessionStorage.getItem("jwtToken")}`,
          };
        }

        const response = await axios({
          method: method,
          url: url,
          headers: headers,
          data: body,
        });
        toast({
          title: "Pari placé avec succès.",
          description: "Retrouver votre pari dans votre espace personnel.",
          status: "success",
          duration: 9000,
          isClosable: true,
        });
        onClose();
        navigate("/allmatchs");
      } catch (error) {
        {
          toast({
            title: "Erreur.",
            description: "Une erreur est survenue.",
            status: "error",
            duration: 9000,
            isClosable: true,
          });
        }
      }
    }
  };

  if (isLoading) {
    return (
      <Box display="flex" justifyContent="center" mt={10}>
        <Spinner
          thickness="10px"
          speed="0.80s"
          emptyColor="white"
          color="primary"
          size="xl"
        />
      </Box>
    );
  }

  return (
    <>
      <Box
        width={{ base: "95%", md: "75%" }}
        margin={"auto"}
        pb={20}
        marginTop={8}
      >
        <Center>
          {isLoggedIn ? (
            <Button
              onClick={onOpen}
              bg="secondary"
              color="white"
              minWidth="200px"
              padding={10}
              mb={10}
              borderWidth={3}
              style={{boxShadow: '4px 8px 18px #F4AAB9'}}
              _hover={{ bg: 'logo' , color:'black' }}
              transition='all 0.2s cubic-bezier(.08,.52,.52,1)'
              isDisabled={statusMatch !== "A venir"}
            >
              {myBet.betDate ? "Modifier mon pari" : "Miser"}
            </Button>
          ) : (
            <Box display={"flex"} alignItems={"baseline"}>
              <LuAlertCircle />
              <Text mb={10} ms={2}>
                Vous devez être connecté pour effectuer un pari.
              </Text>
            </Box>
          )}
        </Center>
        <MatchCard match={match} comments={listComments} />
      </Box>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader fontFamily={"heading"}>
            {match.id_team_home.name} vs {match.id_team_ext.name}
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <BetModalContent
              match={match}
              amount={amount}
              setAmount={setAmount}
              choiceBet={choiceBet}
              setChoiceBet={setChoiceBet}
            />
          </ModalBody>

          <ModalFooter justifyContent={"space-around"}>
            <Button colorScheme="teal" mr={3} onClick={onClose}>
              Annuler
            </Button>
            <Button bg={"secondary"} onClick={saveBet}>
              {myBet.betDate ? "Modifier mon pari" : "Valider le pari"}
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default DetailMatch;
