import "../css/admin.css";
import { useState } from "react";
import {
  Button,
  Box,
  useDisclosure,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useToast,
} from "@chakra-ui/react";
import axios from "axios";
import AddTeam from "../Components/AddTeam";
import AddPlayer from "../Components/AddPlayer";
import AddMatch from "../Components/AddMatch";
import { GiAmericanFootballPlayer } from "react-icons/gi";
import { RiTeamFill } from "react-icons/ri";
import { FaPlusCircle } from "react-icons/fa";
import { RiCalendarScheduleFill } from "react-icons/ri";



const Admin = () => {

  const apiUrl = import.meta.env.VITE_URL_API_SBB_MATCH;
  const now = new Date()
  const tomorrow = now.setDate(now.getDate() + 1)
  const defaultDate = new Date(tomorrow)
  const toast = useToast();
  const [errors, setErrors] = useState({
    id_team_home: false,
    id_team_ext: false,
    start_date: false,
    weather: false,
    quotation_team_home: false,
    quotation_draw: false,
    quotation_team_ext: false,
  });
  const [errorsTeam, setErrorsTeam] = useState({
    name: false,
    id_country: false,
  });
  const [errorsPlayer, setErrorsPlayer] = useState({
    firstname: false,
    lastname: false,
    number : false,
    id_team: false,
  });

  const {
    isOpen: isOpenMatch,
    onOpen: onOpenMatch,
    onClose: onCloseMatch,
  } = useDisclosure();
  const {
    isOpen: isOpenTeam,
    onOpen: onOpenTeam,
    onClose: onCloseTeam,
  } = useDisclosure();
  const {
    isOpen: isOpenPlayer,
    onOpen: onOpenPlayer,
    onClose: onClosePlayer,
  } = useDisclosure();

  const [newTeam, setNewTeam] = useState({
    name: "",
    id_country: "",
  });
  const [newPlayer, setNewPlayer] = useState({
    firstname: "",
    lastname: "",
    number : 1,
    id_team: "",
  });
  const [newMatch, setNewMatch] = useState({
    id_team_home: "",
    id_team_ext: "",
    start_date: defaultDate,
    quotation_team_home: "1.01",
    quotation_team_ext: "1.01",
    quotation_draw: "1.01",
    id_weather: "",
  });

  const validateFormMatch = () => {
    const errors = {};
    if (!newMatch.id_team_home) {
      errors.id_team_home = true;
    }
    if (
      !newMatch.id_team_ext ||
      newMatch.id_team_ext === newMatch.id_team_home
    ) {
      errors.id_team_ext = true;
    }
    if (!newMatch.start_date) {
      errors.start_date = true;
    }
    if (!newMatch.id_weather) {
      errors.id_weather = true;
    }
    if (!newMatch.quotation_team_home) {
      errors.quotation_team_home = true;
    }
    if (!newMatch.quotation_draw) {
      errors.quotation_draw = true;
    }
    if (!newMatch.quotation_team_ext) {
      errors.quotation_team_ext = true;
    }
    setErrors(errors);
    return Object.keys(errors).length === 0;
  };

  const validateFormTeam = () => {
    const errorsTeam = {};
    if (!newTeam.name) {
      errorsTeam.name = true;
    }
    if (!newTeam.id_country) {
      errorsTeam.id_country = true;
    }
    setErrorsTeam(errorsTeam);
    return Object.keys(errorsTeam).length === 0;
  };

  const validateFormPlayer = () => {
    const errorsPlayer = {};
    if (!newPlayer.firstname) {
      errorsPlayer.firstname = true;
    }
    if (!newPlayer.lastname) {
      errorsPlayer.lastname = true;
    }
    if (!newPlayer.number) {
      errorsPlayer.number = true;
    }
    if (!newPlayer.id_team) {
      errorsPlayer.id_team = true;
    }
    setErrorsPlayer(errorsPlayer);
    return Object.keys(errorsPlayer).length === 0;
  };


  const saveMatch = async () => {
    if (validateFormMatch()) {
      try {
        const response = await axios.post(
          `${apiUrl}/api/add_match`,
          { id_team_home: newMatch.id_team_home,
            id_team_ext: newMatch.id_team_ext,
            start_date: newMatch.start_date,
            quotation_team_home: parseFloat(newMatch.quotation_team_home),
            quotation_team_ext: parseFloat(newMatch.quotation_team_ext),
            quotation_draw: parseFloat(newMatch.quotation_draw),
            id_weather: newMatch.id_weather, 
          },
          {
            headers: {
              Authorization: `Bearer ${sessionStorage.getItem("jwtToken")}`,
            },
          }
        );
        toast({
          title: "Succès.",
          description: "Le match a été créé.",
          status: "success",
          duration: 9000,
          isClosable: true,
        });

        onCloseMatch();
      } catch (error) {
        toast({
          title: "Echec.",
          description: "Impossible de créer le match.",
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      }
    }
  };

  const saveTeam = async () => {
    if (validateFormTeam()) {
      try {
        const response = await axios.post(
          `${apiUrl}/api/add_team`,
          { name : newTeam.name, id_country : newTeam.id_country },
          {
            headers: {
              Authorization: `Bearer ${sessionStorage.getItem("jwtToken")}`,
            },
          }
        );
        toast({
          title: "Succès",
          description: "L'équipe a été créé.",
          status: "success",
          duration: 9000,
          isClosable: true,
        });

        onCloseTeam();
      } catch (error) {
        toast({
          title: "Echec",
          description: "Impossible de créer l'équipe",
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      }
    }
  };

  const savePlayer = async () => {
    if (validateFormPlayer()) {
      try {
        const response = await axios.post(
          `${apiUrl}/api/add_player`,
          { firstname: newPlayer.firstname,
            lastname: newPlayer.lastname,
            number : newPlayer.number,
            id_team: newPlayer.id_team},
          {
            headers: {
              Authorization: `Bearer ${sessionStorage.getItem("jwtToken")}`,
            },
          }
        );
        toast({
          title: "Succès",
          description: "Le joueur a été créé.",
          status: "success",
          duration: 9000,
          isClosable: true,
        });

        onClosePlayer();
      } catch (error) {
        toast({
          title: "Echec",
          description: "Impossible de créer le joueur.",
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      }
    }
  };

  return (
    <>
      <Box
        width={{ base: "95%", md: "75%" }}
        className="boxButton"
      >
          <Button
            onClick={onOpenTeam}
            bg="secondary"
            color="white"
            minWidth="200px"
            padding={10}
            mb={10}
            borderWidth={3}
            style={{boxShadow: '4px 8px 18px #F4AAB9'}}
            _hover={{ bg: 'logo', color:'black' }}
            transition='all 0.2s cubic-bezier(.08,.52,.52,1)'
            leftIcon={<FaPlusCircle/>}
            rightIcon={<RiTeamFill/>}
          >
            Créer une équipe
          </Button>
          <Button
            onClick={onOpenPlayer}
            mx={10}
            bg="secondary"
            color="white"
            minWidth="200px"
            padding={10}
            mb={10}
            borderWidth={3}
            style={{boxShadow: '4px 8px 18px #F4AAB9'}}
            _hover={{ bg: 'logo', color:'black' }}
            transition='all 0.2s cubic-bezier(.08,.52,.52,1)'
            leftIcon={<FaPlusCircle/>}
            rightIcon={<GiAmericanFootballPlayer/>}
          >
            Ajouter un joueur
          </Button>
          <Button
            onClick={onOpenMatch}
            bg="secondary"
            color="white"
            minWidth="200px"
            padding={10}
            mb={10}
            borderWidth={3}
            style={{boxShadow: '4px 8px 18px #F4AAB9'}}
            _hover={{ bg: 'logo', color:'black' }}
            transition='all 0.2s cubic-bezier(.08,.52,.52,1)'
            rightIcon={<RiCalendarScheduleFill/>}
          >
            Programmer un match
          </Button>
      </Box>
      <Modal isOpen={isOpenTeam} onClose={onCloseTeam}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader fontFamily={"heading"}>Créer une équipe</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <AddTeam newTeam={newTeam} setNewTeam={setNewTeam} errors={errorsTeam}/>
          </ModalBody>
          <ModalFooter justifyContent={"space-around"}>
            <Button colorScheme="teal" mr={3} onClick={onCloseTeam}>
              Annuler
            </Button>
            <Button bg={"secondary"} onClick={saveTeam}>
              Créer l'équipe
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>

      <Modal isOpen={isOpenPlayer} onClose={onClosePlayer}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader fontFamily={"heading"}>Ajouter un joueur</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <AddPlayer newPlayer={newPlayer} setNewPlayer={setNewPlayer} errors={errorsPlayer}/>
          </ModalBody>
          <ModalFooter justifyContent={"space-around"}>
            <Button colorScheme="teal" mr={3} onClick={onClosePlayer}>
              Annuler
            </Button>
            <Button bg={"secondary"} onClick={savePlayer}>
              Ajouter le joueur
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>

      <Modal isOpen={isOpenMatch} onClose={onCloseMatch}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader fontFamily={"heading"}>Créer un match</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <AddMatch newMatch={newMatch} setNewMatch={setNewMatch} errors={errors} />
          </ModalBody>
          <ModalFooter justifyContent={"space-around"}>
            <Button
              colorScheme="teal"
              mr={3}
              onClick={onCloseMatch}
              errors={errors}
            >
              Annuler
            </Button>
            <Button bg={"secondary"} onClick={saveMatch}>
              Créer le match
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default Admin;
