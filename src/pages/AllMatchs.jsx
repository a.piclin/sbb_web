import axios from "axios";
import { useState, useEffect } from "react";
import {
  Box,
  Heading,
  Text,
  useToast,
  Spinner,
  Center,
} from "@chakra-ui/react";
import ListMatch from "../Components/ListMatch";
import Pagination from "../Components/Pagination";

const AllMatchs = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [hasError, setHasError] = useState(false);
  const toast = useToast();
  const [listMatchs, setListMatchs] = useState("");
  const [page, setPage] = useState(1);
  const [totalItems, setTotalItems] = useState(0);
  const itemsPerPage = 10;
  const apiUrl = import.meta.env.VITE_URL_API_SBB_MATCH;

  const getMatchs = async () => {
    setHasError(false);
    try {
      const response = await axios.get(
        `${apiUrl}/api/get_all_matchs?page=${page}`
      );

      setListMatchs(response.data["hydra:member"]);
      setTotalItems(response.data['hydra:totalItems']);
      setIsLoading(false);
    } catch (error) {
      {
        toast({
          title: "Erreur.",
          description: "Une erreur est survenue.",
          status: "error",
          duration: 9000,
          isClosable: true,
        });
        setIsLoading(false);
        setHasError(true);
      }
    }
  };

  useEffect(() => {
    getMatchs();
  }, [page]);

  const totalPages = Math.ceil(totalItems / itemsPerPage);

  if (isLoading) {
    return (
      <Box display="flex" justifyContent="center" mt={10}>
        <Spinner
          thickness="10px"
          speed="0.80s"
          emptyColor="white"
          color="primary"
          size="xl"
        />
      </Box>
    );
  }

  return (
    <Box width={{ base: "90%", md: "50%" }} margin={"auto"} pb={20}>
      <Heading as="h2" my={6} className="centerText">
        Les matchs
      </Heading>
      {hasError ? <Text className="explication">No data</Text> : <ListMatch listMatchs={listMatchs}/>}

      <Center mt={4}>
            <Pagination page={page} setPage={setPage} totalPages={totalPages} />
      </Center>
    </Box>
  );
};

export default AllMatchs;
