import { extendTheme } from "@chakra-ui/react";

const theme = extendTheme({
  config: {
    initialColorMode: "light",
    useSystemColorMode: false,
  },
  colors: {
    primary : "#3F88C5",
    secondary: "#fe646f",
    tertiary: "#C5B270", 
    logo: "#8bff70"
  },
  fonts: {
    heading: "'Permanent Marker', sans-serif",
    body: "'Roboto', sans-serif",
  },
});

export default theme;