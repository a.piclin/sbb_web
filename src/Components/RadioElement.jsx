import { Box, useRadio } from "@chakra-ui/react"


const RadioElement = (props) => {
    const { getInputProps, getRadioProps } = useRadio(props)
  
    const input = getInputProps()
    const checkbox = getRadioProps()
  
    return (
      <Box as='label'>
        <input {...input} />
        <Box
          {...checkbox}
          cursor='pointer'
          borderWidth='2px'
          borderRadius='md'
          boxShadow='md'
          _checked={{
            bg: 'logo',
            color: 'primary',
            borderColor: 'primary',
            borderWidth : '3px'
          }}
          px={5}
          py={3}
        >
          {props.children}
        </Box>
      </Box>
    )
  }

  export default RadioElement