import BetModalContent from "./BetModalContent";
import { Box, Center, Divider, Text } from "@chakra-ui/react";
import { useEffect } from "react";

const MultipleBetModalContent = ({ listSelect, bets, setBets }) => {

  const handleChoiceBetChange = (index, value) => {
    setBets((prevBets) =>
      prevBets.map((bet, i) =>
        i === index ? { ...bet, choiceBet: value } : bet
      )
    );
  };

  const handleAmountChange = (index, value) => {
    setBets((prevBets) =>
      prevBets.map((bet, i) => (i === index ? { ...bet, amount: value } : bet))
    );
  };

  const formatTime = (date) => {
    const options = {
      weekday: "long",
      year: "numeric",
      month: "long",
      day: "numeric",
      hour: "2-digit",
      minute: "2-digit",
    };
    return new Date(date).toLocaleDateString("fr-FR", options);
  };

  useEffect(() => {
    const initialBets = Array.from(listSelect.values()).map((match) => ({
      matchId: match.id,
      amount: 0.5,
      choiceBet: "Victoire domicile",
    }));
    setBets(initialBets);
  }, [listSelect, setBets]);

  return (
    <>
      {Array.from(listSelect.values()).map((match, index) =>
          <Box>
            <Divider />
            <Center>
              <Text my={2}>{formatTime(match.start_date).toUpperCase()}</Text>
            </Center>
            <Center>
              <Text fontFamily={"heading"} my={2}>
                {match.id_team_home.name} vs {match.id_team_ext.name}
              </Text>
            </Center>
            <BetModalContent
            match={match}
            amount={bets[index]?.amount || 0.5}
            setAmount={(value) => handleAmountChange(index, value)}
            choiceBet={bets[index]?.choiceBet || "Victoire domicile"}
            setChoiceBet={(value) => handleChoiceBetChange(index, value)}
          />
          </Box>
      )}
    </>
  );
};

export default MultipleBetModalContent;
