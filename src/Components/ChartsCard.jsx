import { Box, Heading, Badge, Center, Divider, Text } from "@chakra-ui/react";
import {
  ResponsiveContainer,
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ReferenceLine,
  Legend,
} from "recharts";
import { format } from "date-fns";
import CustomizedAxisTick from "./CustomizedAxisTick";
import { TiInfo } from "react-icons/ti";

const ChartsCard = ({ data }) => {

  const defaultData =   [{
    "Solde" : 0
  }]

  const filteredData = data.filter((item) => item.calculatedEarnings !== null);

  const dailyBalance = filteredData.reduce((acc, curr) => {
    const newDate = new Date(curr.end_date.date);
    const date = format(newDate, "yyyy-MM-dd");
    acc[date] = (acc[date] || 0) + curr.calculatedEarnings;
    return acc;
  }, {});

  let cumulativeBalance = 0;
  const chartData = Object.keys(dailyBalance)
    .sort()
    .map((date) => {
      cumulativeBalance += dailyBalance[date];
      return { date, Solde: cumulativeBalance };
    });

  const finalData = chartData.length > 0 ? chartData : defaultData;

  const soldeValues = finalData.map((item) => Math.abs(item.Solde));
  const maxSolde = Math.max(...soldeValues, 20);

  return (
    <Box
      p={4}
      borderWidth={1}
      borderRadius="md"
      minHeight={"70vh"}
      width="100%"
      bg={"white"}
    >
      <Center mb={3}>
        <Badge fontSize={"md"}>Dashboard</Badge>
      </Center>
      <Divider orientation="horizontal" borderWidth={"2px"} mb={5} />
      <Heading as="h1" size="lg" mb={5}>
        Historique du solde{" "}
      </Heading>
      <ResponsiveContainer width="100%" height={500}>
        <LineChart data={chartData}>
          <CartesianGrid strokeDasharray="6 6" />
          <XAxis
            dataKey="date"
            padding={{ left: 15 }}
            tick={<CustomizedAxisTick />}
            height={80}
          />
          <YAxis domain={[-maxSolde, maxSolde]} />
          <Tooltip />
          <ReferenceLine y={0} stroke="black" strokeWidth={2.5} />
          <Legend />
          <Line
            type="monotone"
            dataKey="Solde"
            stroke="#fe646f"
            strokeWidth={2}
          />
        </LineChart>
      </ResponsiveContainer>
      <Box display={"flex"}>
        <TiInfo />{" "}
        <Text as="cite" ms={2}>
          {" "}
          Prends en compte uniquement les paris terminés
        </Text>
      </Box>
    </Box>
  );
};

export default ChartsCard;
