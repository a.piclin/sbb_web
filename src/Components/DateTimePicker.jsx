import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { FormControl, Input } from "@chakra-ui/react";
import { registerLocale } from  "react-datepicker";
import { fr } from 'date-fns/locale/fr';
registerLocale('fr', fr)

const DateTimePicker = ({ selectedDate, onChange }) => {
  
  const now = new Date();
  
  const filterPassedTime = (time) => {
    const currentDate = new Date();
    currentDate.setHours(0, 0, 0, 0);
    return currentDate.getTime() !== selectedDate.setHours(0, 0, 0, 0) || time >= now;
  };

  return (
    <FormControl>
      <DatePicker
        selected={selectedDate}
        onChange={onChange}
        showTimeSelect
        timeFormat="HH:mm"
        timeIntervals={15}
        dateFormat="Pp"
        timeCaption="Heure"
        customInput={<Input />}
        locale="fr"
        minDate={now}
        filterTime={filterPassedTime}
      />
    </FormControl>
  );
};

export default DateTimePicker;
