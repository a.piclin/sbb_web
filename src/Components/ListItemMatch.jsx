import {
  Box,
  Text,
  HStack,
  Badge,
  Center,
  Button
} from "@chakra-ui/react";
import { IoAddCircleOutline } from "react-icons/io5";
import { useNavigate } from "react-router-dom";

const ListItemMatch = ({ match }) => {
  const navigate = useNavigate();

  const getStatus = (status, start_date) => {
    if (status === "A venir") {
      const now = new Date();
      if (now > new Date(start_date)) {
        return "En cours";
      } else {
        return status;
      }
    } else {
      return status;
    }
  };

  const getStatusColor = (status) => {
    if (status === "En cours") {
      return "green";
    } else if (status === "A venir") {
      return "purple";
    } else {
      return "red";
    }
  };

  const formatTime = (date) => {
    const options = {
      weekday: "long",
      year: "numeric",
      month: "long",
      day: "numeric",
      hour: "2-digit",
      minute: "2-digit",
    };
    return new Date(date).toLocaleDateString("fr-FR", options);
  };

  const status = getStatus(match.status, match.start_date);

  const statusColor = getStatusColor(status);

  return (
      <Box
        key={match.id}
        p={4}
        borderWidth={1}
        borderRadius="md"
        width="100%"
        className={status !== "En cours" ? "boxDisabled" : "boxActive"}
      >
        <Center mb={3}>
          <Badge colorScheme={statusColor}>{status}</Badge>
        </Center>
        <Text fontFamily={"heading"} textAlign={"center"} mb={5}>
          {formatTime(match.start_date).toUpperCase()}
        </Text>
        <HStack justifyContent={"space-around"} mb={3}>
          <Box>
            {match.id_team_home.name} ({match.id_team_home.id_country.code})
            {status !== "A venir" ? (
              <Text className="centerText" fontFamily={"heading"} fontSize={28}>
                {match.score_team_home}
              </Text>
            ) : (
              <Text className="centerText" fontFamily={"heading"} fontSize={28}>
                -
              </Text>
            )}
          </Box>
          <Box>
            <Text fontFamily={"heading"} fontSize={28}>
              Vs
            </Text>
          </Box>
          <Box>
            {match.id_team_ext.name} ({match.id_team_ext.id_country.code})
            {status !== "A venir" ? (
              <Text className="centerText" fontFamily={"heading"} fontSize={28}>
                {match.score_team_ext}
              </Text>
            ) : (
              <Text className="centerText" fontFamily={"heading"} fontSize={28}>
                -
              </Text>
            )}
          </Box>
        </HStack>
        <Center>
          <Button
            leftIcon={<IoAddCircleOutline />}
            onClick={() => navigate("../details/" + match.id)}
            bg="secondary"
            color="white"
            borderWidth={3}
            style={{boxShadow: '4px 8px 18px #F4AAB9'}}
            _hover={{ bg: 'logo', color:'black' }}
            transition='all 0.2s cubic-bezier(.08,.52,.52,1)'
          >
            Détails
          </Button>
        </Center>
      </Box>
  );
};

export default ListItemMatch;
