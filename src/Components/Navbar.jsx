import "../css/navbar.css";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  Button,
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
  IconButton,
  useDisclosure,
  List,
  ListItem,
  useToast,
  Box,
} from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import {  HamburgerIcon } from "@chakra-ui/icons";
import { useContext } from "react";
import AppContext from "../contexts/AppContext";
import { IoLogIn, IoPersonAddSharp, IoLogOut } from "react-icons/io5";
import { FaFootball } from "react-icons/fa6";
import { MdAccountCircle, MdAdminPanelSettings } from "react-icons/md";

const Navbar = () => {
  const navigate = useNavigate();
  const toast = useToast();
  const { isLoggedIn, setIsLoggedIn, hasRole, decodeToken } = useContext(AppContext);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const logout = () => {
    setIsLoggedIn(false);
    sessionStorage.removeItem("jwtToken");
    toast({
      title: "Déconnexion réussie.",
      description: "Merci pour votre visite.",
      status: "info",
      duration: 9000,
      isClosable: true,
    });
    navigate("/login");
    onClose();
  };

  return (
    <>
      {/* Hamburger icon for small screens */}
      <IconButton
        aria-label="Open Menu"
        icon={<HamburgerIcon />}
        display={{ base: "block", lg: "none" }}
        onClick={onOpen}
      />

      {/* Drawer for small screens */}
      <Drawer placement="left" onClose={onClose} isOpen={isOpen}>
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton />
          <DrawerHeader>Menu</DrawerHeader>
          <DrawerBody>
            <List spacing={4} className="blocLink">
              <ListItem>
                <Button
                  variant="link"
                  onClick={() => {
                    navigate("/");
                    onClose();
                  }}
                >
                  Accueil
                </Button>
              </ListItem>
              <ListItem>
                <Button
                  variant="link"
                  onClick={() => {
                    navigate("/allmatchs");
                    onClose();
                  }}
                >
                  Les matchs
                </Button>
              </ListItem>
              <ListItem>
                <Button
                  variant="link"
                  onClick={() => {
                    navigate("/bets");
                    onClose();
                  }}
                >
                  Parier
                </Button>
              </ListItem>
              {isLoggedIn ? (
                <>
                {hasRole(decodeToken(sessionStorage.getItem("jwtToken")).roles, 'ROLE_ADMIN') ? (  <ListItem>
                  <Button
                      variant="link"
                      onClick={() => {
                        navigate("/admin");
                        onClose();
                      }}
                      rightIcon={<MdAdminPanelSettings />} 
                    >
                      Administrateur
                    </Button>
                  </ListItem>) : (  <ListItem>
                    <Button
                      variant="link"
                      onClick={() => {
                        navigate("/myspace");
                        onClose();
                      }}
                      rightIcon={<MdAccountCircle />} 
                    >
                      Mon espace
                    </Button>
                  </ListItem>)}
                  <ListItem>
                    <Button onClick={logout}
                    rightIcon={<IoLogOut />}
                    >Déconnexion</Button>
                  </ListItem>
                </>
              ) : (
                <>
                  <ListItem>
                    <Button
                      variant="link"
                      onClick={() => {
                        navigate("/registration");
                        onClose();
                      }}
                      rightIcon={<IoPersonAddSharp />}
                    >
                      Inscription
                    </Button>
                  </ListItem>
                  <ListItem>
                    <Button
                      variant="link"
                      onClick={() => {
                        navigate("/login");
                        onClose();
                      }}
                      rightIcon={<IoLogIn />}
                    >
                      Connexion
                    </Button>
                  </ListItem>
                </>
              )}
            </List>
          </DrawerBody>
        </DrawerContent>
      </Drawer>

      {/* Breadcrumb navigation for larger screens */}
      <Box
        display={{ base: "none", lg: "flex" }}
        bg={"logo"}
        color={"primary"}
        className="navbar"
        maxW="100vw" 
        overflowX="hidden"
      >
        <Box>
        <Breadcrumb
          spacing="15px"
          separator={<FaFootball color="#C5B270" size={25} />}
        >

          <BreadcrumbItem>
            <BreadcrumbLink href="/" ms={2} fontFamily={"heading"} fontSize={{md :"22px", lg : "28px"}} color="black">Accueil</BreadcrumbLink>
          </BreadcrumbItem>

          <BreadcrumbItem>
            <BreadcrumbLink href="/allmatchs" fontFamily={"heading"} fontSize={{md :"22px", lg : "28px"}} color="black">
              Les matchs
            </BreadcrumbLink>
          </BreadcrumbItem>

          <BreadcrumbItem>
            <BreadcrumbLink href="/bets" fontFamily={"heading"} fontSize={{md :"22px", lg : "28px"}} color="black">Parier</BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumb>
        </Box>

        <Box>
        {isLoggedIn ? (
          <>
            {hasRole(decodeToken(sessionStorage.getItem("jwtToken")).roles, 'ROLE_ADMIN') ? (<Button onClick={() => navigate("/admin")} rightIcon={<MdAdminPanelSettings />}  me={4}>Administrateur</Button>) : (<Button onClick={() => navigate("/myspace")} rightIcon={<MdAccountCircle />}  me={4}>Mon espace</Button>) }

            <Button onClick={logout}  me={4} rightIcon={<IoLogOut />} >Déconnexion</Button>
          </>
        ) : (
          <>
            <Button onClick={() => navigate("/registration")} me={4} rightIcon={<IoPersonAddSharp />}>Inscription</Button>

            <Button onClick={() => navigate("/login")} me={4}  rightIcon={<IoLogIn />}>Connexion</Button>
          </>
        )}
        </Box>
      </Box>
    </>
  );
};

export default Navbar;
