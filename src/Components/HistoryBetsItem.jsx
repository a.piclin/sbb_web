import { Box, Text, HStack, Divider, Badge, Center } from "@chakra-ui/react";

const HistoryBetsItem = ({ bet }) => {
  const formatTimeDayAndTime = (date) => {
    const options = {
      weekday: "short",
      year: "numeric",
      month: "short",
      day: "numeric",
      hour: "2-digit",
      minute: "2-digit",
    };
    return new Date(date).toLocaleDateString("fr-FR", options);
  };

  return (
    <>
    <Box  width="100%">
      <Center>
        <Badge fontSize={'lg'} bgColor={"secondary"}>{formatTimeDayAndTime(bet.betDate)}</Badge>
      </Center>
      <Box
        key={bet.id}
        p={4}
        borderWidth={1}
        borderRadius="md"
        mb={5}
        backgroundColor={"#eeeeee"}
      >
        <Box mb={4}>
          <Center>
            <Text>
              Pari : <Badge variant={'outline'} fontSize='md'>{bet.bet}</Badge>
            </Text>
          </Center>
          <Center>
            <Text mt={2}>
              Montant : <Badge variant={'outline'} fontSize='md'>{bet.amount} €</Badge>
            </Text>
          </Center>
        </Box>
        <Divider orientation="horizontal" borderWidth={"2px"} borderColor={'white'}/>
        <HStack
          justifyContent={"space-around"}
          mb={3}
          fontFamily={"heading"}
          fontSize={22}
        >
          <Box>{bet.name_team_home}</Box>
          <Box>
            <Text fontSize={28}>Vs</Text>
          </Box>
          <Box>{bet.name_team_ext}</Box>
        </HStack>
        <HStack justifyContent={"space-around"} mb={3} fontSize={14}>
          <Text textAlign={"center"}>
            Début : {formatTimeDayAndTime(bet.start_date.date).toUpperCase()}
          </Text>
          <Text textAlign={"center"}>
            Fin : {formatTimeDayAndTime(bet.end_date.date).toUpperCase()}
          </Text>
        </HStack>
        <Divider orientation="horizontal" borderWidth={"2px"} borderColor={'white'}/>
        <Box mt={4}>
        <Center>
          <Text>
            Résultat : <Badge colorScheme="purple" fontSize={'md'}>{bet.final_result}</Badge>
          </Text>
          </Center>
          <Center>
          <Text mt={2}>
            Solde du pari :
            <Badge ms={2} variant='subtle' fontSize='md' colorScheme={bet.calculatedEarnings < 0 ? "red" : "green"}>
              {bet.calculatedEarnings === null
                ? "-"
                : bet.calculatedEarnings + " €"}{" "}
            </Badge>
          </Text>
          </Center>
        </Box>
      </Box>
      </Box>
    </>
  );
};

export default HistoryBetsItem;
