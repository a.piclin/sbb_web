import { useEffect, useState } from "react";
import { Select, useToast } from "@chakra-ui/react";
import axios from "axios";

const SelectWeather = ({ setNewMatch }) => {
  const toast = useToast();
  const [listWeather, setListWeather] = useState([]);
  const apiUrl = import.meta.env.VITE_URL_API_SBB_MATCH;

  const getListWeather = async () => {
    try {
      const response = await axios.get(
        `${apiUrl}/api/get_all_weathers`
      );
      setListWeather(response.data["hydra:member"]);
    } catch (error) {
      {
        toast({
          title: "Erreur.",
          description: "Une erreur est survenue.",
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      }
    }
  };

  useEffect(() => {
    getListWeather();
  }, []);

  return (
    <Select
      placeholder="Choisir la météo"
      onChange={(e) => {
        setNewMatch((prevState) => ({
          ...prevState,
          id_weather: e.target.value,
        }));
      }}
    >
      {listWeather.map((weather) => (
        <option value={weather["@id"]} key={weather["@id"]}>
          {weather.label}
        </option>
      ))}
    </Select>
  );
};

export default SelectWeather;
