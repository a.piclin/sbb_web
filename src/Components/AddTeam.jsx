import { Input, FormLabel, FormControl } from "@chakra-ui/react";
import SelectCountries from "./SelectCountries";

const AddTeam = ({ newTeam, setNewTeam, errors }) => {
  const handleSetNewCountry = (value) => {
    setNewTeam((prevState) => ({
      ...prevState,
      id_country: value,
    }));
  };

  return (
    <>
      <FormControl isRequired isInvalid={errors.name}>
        <FormLabel>Nom de l'équipe</FormLabel>
        <Input
          type={"text"}
          value={newTeam.name}
          onChange={(event) => {
            setNewTeam((prevState) => ({
              ...prevState,
              name: event.target.value,
            }));
          }}
          isRequired
          mb={4}
        />
      </FormControl>

      <FormControl isRequired isInvalid={errors.id_country}>
        <FormLabel>Pays</FormLabel>
        <SelectCountries setNewCountry={handleSetNewCountry} />
      </FormControl>
    </>
  );
};

export default AddTeam;
