import { useState } from "react";
import {
  Box,
  Badge,
  Divider,
  HStack,
  IconButton,
  useToast,
  useBreakpointValue,
  Center,
  Text,
  useDisclosure,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button
} from "@chakra-ui/react";
import HistoryBetsItem from "./HistoryBetsItem";
import { MdDeleteForever } from "react-icons/md";
import { TiEdit } from "react-icons/ti";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const HistoryCard = ({ data }) => {
  const navigate = useNavigate();
  const toast = useToast();
  const isBase = useBreakpointValue({ base: true, md: false });
  const [idDelete, setIdDelete] = useState(null);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const apiUrl = import.meta.env.VITE_URL_API_SBB_MATCH;

  const getLastPartOfString = (str) => {
    const segments = str.split('/');
    const lastPart = segments.splice(-1)[0];
    return lastPart;
  }

  const handleClose = () => {
    onClose();
    setIdDelete(null);
  };

  const askDeleteBet = (id) => {
    setIdDelete(getLastPartOfString(id));
    onOpen()
  };

  const deleteBet = async () => {
    try {
      const response = await axios.delete(`${apiUrl}/api/delete_bet/${idDelete}`, {
        headers: {
          Authorization: `Bearer ${sessionStorage.getItem("jwtToken")}`,
        },
      })
      toast({
        title: "Suppression réussie.",
        description: "Votre pari a été annulé.",
        status: "success",
        duration: 9000,
        isClosable: true,
      });
      setIdDelete(null)
      onClose()
    } catch (error) {
      {
        toast({
          title: 'Erreur.',
          description: 'Une erreur est survenue.',
          status: 'error',
          duration: 9000,
          isClosable: true
        })
      }
    }
      setIdDelete(null)
      onClose()
  }

  return (
    <Box
      p={4}
      borderWidth={1}
      borderRadius="md"
      minHeight={"70vh"}
      width="100%"
      bg={"white"}
    >
      <Center mb={3}>
        <Badge fontSize={"md"}>Historique</Badge>
      </Center>
      <Divider orientation="horizontal" borderWidth={"2px"} mb={5} />

      <>
        {data.length === 0 && <Center><Text>Aucun pari</Text></Center>}

        {data.length > 0 && data.map((bet) => (
          <>
            <HStack key={bet.id}>
              <HistoryBetsItem bet={bet} />

              {!isBase && (
                <Box>
                  <IconButton
                    aria-label="Supprimer"
                    colorScheme="red"
                    icon={<MdDeleteForever />}
                    onClick={() => askDeleteBet(bet.id)}
                    isDisabled={bet.final_result != "En attente"}
                    mb={4}
                  />
                  <IconButton
                    aria-label="Modifier"
                    backgroundColor="logo"
                    icon={<TiEdit />}
                    onClick={() => navigate(`/details/${bet.id_match}`)}
                  />
                </Box>
              )}
            </HStack>
            {isBase && (
              <>
              <Box mb={4} display={'flex'} justifyContent={'space-around'}>
                <IconButton
                  aria-label="Supprimer"
                  colorScheme="red"
                  icon={<MdDeleteForever />}
                  onClick={() => askDeleteBet(bet.id)}
                  isDisabled={bet.final_result != "En attente"}
                />
                <IconButton
                  aria-label="Modifier"
                  backgroundColor="logo"
                  icon={<TiEdit />}
                  onClick={() => navigate(`/details/${bet.id_match}`)}
                />
              </Box>
              <Divider orientation="horizontal" borderWidth={"2px"} mb={5} />
              </>
            )}
          </>
        ))}
      </>
      
      <Modal isOpen={isOpen} onClose={handleClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader fontFamily={"heading"}>Supprimer</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
              <Text> Souhaitez-vous vraiment supprimer définitivement votre pari ?</Text>
          </ModalBody>
          <ModalFooter justifyContent={"space-around"}>
            <Button colorScheme="teal" mr={3} onClick={handleClose}>
              Annuler
            </Button>
            <Button bg={"secondary"} onClick={() => {deleteBet()}}>
              Oui
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Box>
  );
};

export default HistoryCard;
