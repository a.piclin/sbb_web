import { useEffect, useState } from 'react'
import { Select, useToast } from '@chakra-ui/react'
import axios from 'axios';

const SelectTeam = ({setTeam}) => {
    const toast = useToast();
    const [listTeam, setListTeam] = useState([]);
    const apiUrl = import.meta.env.VITE_URL_API_SBB_MATCH;

    const getListTeam = async () => {
        try {
          const response = await axios.get(`${apiUrl}/api/get_all_team`)
          setListTeam(response.data['hydra:member'])
        } catch (error) {
          {
            toast({
              title: 'Erreur.',
              description: 'Une erreur est survenue.',
              status: 'error',
              duration: 9000,
              isClosable: true
            })
          }
        }
      }
    
      useEffect(() => {
        getListTeam()
      }, [])

  return (
    <Select placeholder='Choisir une équipe' onChange={(e) => {setTeam(e.target.value)} }>
    {listTeam.map((team) => (
          <option value={team['@id']} key={team['@id']}>{team.name}</option>
    ))}
    </Select>
  )
}

export default SelectTeam;