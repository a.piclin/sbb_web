import "../css/usercard.css";
import { Box, Text, Badge, Center, Divider, Avatar } from "@chakra-ui/react";
import { HiUser } from "react-icons/hi2";


const UserInfoCard = ({data}) => {

  const capitalize = (word) => {
    if (typeof word !== 'string') {
      return null
    }
    return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
  }


  return (
    <Box p={4} borderWidth={1} borderRadius="md" width="100%" bg={'white'}>
      <Center mb={3}>
        <Badge fontSize={'md'}>Mes informations</Badge>
      </Center>
      <Divider orientation="horizontal" borderWidth={"2px"} mb={5} />
    <Center>
      <Box className="backgroundPlayer">
      <Box className="infos">
        <Avatar size="xl" mr={4} bg='secondary' icon={<HiUser fontSize='3rem'/>}/>
          <Box display={'flex'} flexDirection={'column'} justifyContent={'center'}>
          <Text fontWeight="bold" fontSize="xl" color={'white'} >
            {data.lastname && `${capitalize(data.firstname)} ${data.lastname.toUpperCase()}`}
          </Text>
          <Text fontSize="md" color={'secondary'}>
          {data.email}
          </Text>
          </Box>
        </Box>
      </Box>
    </Center>


    </Box>
  );
};

export default UserInfoCard;
