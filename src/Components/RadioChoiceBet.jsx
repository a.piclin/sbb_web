import RadioElement from "./RadioElement"
import { HStack, useRadioGroup } from "@chakra-ui/react"

function RadioChoiceBet({choiceBet, setChoiceBet}) {
  const options = ['Victoire domicile', 'Match nul', 'Victoire extérieur']

  const { getRootProps, getRadioProps } = useRadioGroup({
    name: 'choiceBet',
    defaultValue: choiceBet,
    onChange: (value) => setChoiceBet(value),
  })

  const group = getRootProps()

  return (
    <HStack {...group}>
      {options.map((value) => {
        const radio = getRadioProps({ value })
        return (
          <RadioElement key={value} {...radio}>
            {value}
          </RadioElement>
        )
      })}
    </HStack>
  )
}

export default RadioChoiceBet;