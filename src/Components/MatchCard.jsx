import { Box, Text, HStack, Badge, Center, Divider, VStack } from "@chakra-ui/react";
import { GiAmericanFootballPlayer } from "react-icons/gi";
import QuotationCard from "./QuotationCard";

const MatchCard = ({ match, comments }) => {

  const getStatus = (status, start_date) => {
    if (status === "A venir") {
      const now = new Date();
      if (now > new Date(start_date)) {
        return "En cours";
      } else {
        return status;
      }
    } else {
      return status;
    }
  };

  const getStatusColor = (status) => {
    if (status === "En cours") {
      return "green";
    } else if (status === "A venir") {
      return "purple";
    } else {
      return "red";
    }
  };

  const formatTimeDay = (date) => {
    const options = {
      weekday: "long",
      year: "numeric",
      month: "long",
      day: "numeric",
    };
    return new Date(date).toLocaleDateString("fr-FR", options);
  };

  const formatTimeHour = (dateString) => {
    const date = new Date(dateString);
    return date.toLocaleTimeString("fr-FR", {
      hour: "2-digit",
      minute: "2-digit",
    });
  };

  const formatTimeDayAndHour = (date) => {
    const options = {
      year: 'numeric',
      month: 'numeric',
      day: 'numeric',
      hour: '2-digit',
      minute: '2-digit'
    }
    return new Date(date).toLocaleDateString('fr-FR', options)
  }

  const status = getStatus(match.status, match.start_date);

  const statusColor = getStatusColor(status);

  return (
    <>
      <Box
        key={match.id}
        p={4}
        borderWidth={1}
        borderRadius="md"
        width="100%"
        className={status !== "En cours" ? "boxDisabled" : "boxActive"}
      >
        <Center mb={3}>
          <Badge colorScheme={statusColor}>{status}</Badge>
        </Center>
        <Text fontFamily={"heading"} textAlign={"center"} mb={3}>
          {formatTimeDay(match.start_date).toUpperCase()}
        </Text>
        <HStack justifyContent={"space-around"} mb={3}>
        <Text fontFamily={"heading"} textAlign={"center"}>
          Début : {formatTimeHour(match.start_date).toUpperCase()}
        </Text>
        <Text fontFamily={"heading"} textAlign={"center"}>
          Fin : {formatTimeHour(match.end_date).toUpperCase()}
        </Text>
        </HStack>
        <Center mb={3}>
          <Text>
            Météo : <Badge>{match.id_weather.label}</Badge>
          </Text>
        </Center>
        <Divider orientation='horizontal' borderWidth={'2px'} mb={5}/>
        <Center mb={3}>
          <QuotationCard match={match}/>
        </Center>
        <Divider orientation='horizontal' borderWidth={'2px'} mb={5}/>
        <HStack justifyContent={"space-around"} mb={3}>
          <Box>
            {match.id_team_home.name} ({match.id_team_home.id_country.name})
            {status !== "A venir" ? (
              <Text className="centerText" fontFamily={"heading"} fontSize={28}>
                {match.score_team_home}
              </Text>
            ) : (
              <Text className="centerText" fontFamily={"heading"} fontSize={28}>
                -
              </Text>
            )}
          </Box>
          <Box>
            <Text fontFamily={"heading"} fontSize={28}>
              Vs
            </Text>
          </Box>
          <Box>
            {match.id_team_ext.name} ({match.id_team_ext.id_country.name})
            {status !== "A venir" ? (
              <Text className="centerText" fontFamily={"heading"} fontSize={28}>
                {match.score_team_ext}
              </Text>
            ) : (
              <Text className="centerText" fontFamily={"heading"} fontSize={28}>
                -
              </Text>
            )}
          </Box>
        </HStack>
        <Divider orientation='horizontal' borderWidth={'2px'} />
        <HStack justifyContent={"space-around"} mb={3}>
          <Box>
            <Text mb={5}>Nb joueurs : {match.id_team_home.players.length}</Text>

            {match.id_team_home.players.map((player) => (
              <Box display={"flex"} flexDirection={"row"} key={`playerDom${player.firstname}`}>
                <GiAmericanFootballPlayer />
                <Text ms={3}>
                  {player.firstname.toUpperCase()}{" "}
                  {player.lastname.toUpperCase()}
                </Text>
                <Text ms={2} fontFamily={'heading'}>N°{" "}{player.number ?? '??'}</Text>
              </Box>
            ))}
          </Box>
          <Box height={'80px'}><Divider orientation='vertical' borderWidth={'2px'} /></Box>
          <Box>
            <Text mb={5}>Nb joueurs : {match.id_team_ext.players.length}</Text>

            {match.id_team_ext.players.map((player) => (
              <Box display={"flex"} flexDirection={"row"} key={`playerExt${player.firstname}`}>
                <GiAmericanFootballPlayer/>
                <Text ms={3}>
                  {player.firstname.toUpperCase()}{" "}
                  {player.lastname.toUpperCase()}
                </Text>
                <Text ms={2} fontFamily={'heading'}>N°{" "}{player.number ?? '??'}</Text>
              </Box>
            ))}
          </Box>
        </HStack>
        <Divider orientation='horizontal' borderWidth={'2px'} />
        <VStack spacing={4}>
          {comments.length === 0 && <Text mt={5}><Badge fontSize={'lg'}>Aucun commentaires</Badge></Text>}
          {comments.map((comment) => (
            <HStack width={'100%'} key={`blocComment${comment.id}`}>
              <Box
                key={`comment${comment.id}`}
                p={4}
                borderWidth={1}
                borderRadius="md"
                width="100%"
                bg={status !== 'En cours' ? 'white' : 'gray.200'}
              >
                <Center mb={3}>
                  <Badge>{formatTimeDayAndHour(comment.date)}</Badge>
                </Center>
                <Text textAlign={'center'}>{comment.content}</Text>
              </Box>
            </HStack>
          ))}
        </VStack>
      </Box>
    </>
  );
};

export default MatchCard;
