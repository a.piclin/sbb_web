import {
  Input,
  FormLabel,
  FormControl,
  FormErrorMessage,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  Box
} from "@chakra-ui/react";
import SelectTeam from "./SelectTeam";

const AddPlayer = ({ newPlayer, setNewPlayer, errors }) => {
  const handleSetTeam = (value) => {
    setNewPlayer((prevState) => ({
      ...prevState,
      id_team: value,
    }));
  };

  return (
    <>
      <FormControl isRequired isInvalid={errors.firstname}>
        <FormLabel>Nom</FormLabel>
        <Input
          mb={3}
          type="text"
          placeholder="Nom"
          value={newPlayer.firstname}
          onChange={(e) => {
            setNewPlayer((prevState) => ({
              ...prevState,
              firstname: e.target.value,
            }));
          }}
        />
        <FormErrorMessage>Champ requis.</FormErrorMessage>
      </FormControl>

      <FormControl isRequired isInvalid={errors.lastname}>
        <FormLabel>Prénom</FormLabel>
        <Input
          mb={3}
          type="text"
          placeholder="Prénom"
          value={newPlayer.lastname}
          onChange={(e) => {
            setNewPlayer((prevState) => ({
              ...prevState,
              lastname: e.target.value,
            }));
          }}
        />
        <FormErrorMessage>Champ requis.</FormErrorMessage>
      </FormControl>

      <FormControl isRequired isInvalid={errors.number}>
        <FormLabel>Numéro</FormLabel>
        <Box mb={3}>
          <NumberInput
            defaultValue={1}
            onChange={(value) => {
              setNewPlayer((prevState) => ({
                ...prevState,
                number: parseInt(value),
              }));
            }}
            min={1}
            step={1}
            precision={0}
          >
            <NumberInputField />
            <NumberInputStepper>
              <NumberIncrementStepper />
              <NumberDecrementStepper />
            </NumberInputStepper>
          </NumberInput>
        </Box>
        <FormErrorMessage>Champ requis.</FormErrorMessage>
      </FormControl>

      <FormControl isRequired isInvalid={errors.id_team}>
        <FormLabel>Équipe</FormLabel>
        <SelectTeam setTeam={handleSetTeam} />
        <FormErrorMessage>Champ requis.</FormErrorMessage>
      </FormControl>
    </>
  );
};

export default AddPlayer;
