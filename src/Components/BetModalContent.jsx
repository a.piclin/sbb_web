import {
  Box,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  Text,
} from "@chakra-ui/react";
import QuotationCard from "../Components/QuotationCard.jsx";
import RadioChoiceBet from "./RadioChoiceBet.jsx";

const BetModalContent = ({
  match,
  amount,
  setAmount,
  choiceBet,
  setChoiceBet,
}) => {
  return (
    <Box>
        <Box>
      <QuotationCard match={match}/>
      </Box>
      <Box my={'30px'}>
      <NumberInput defaultValue={amount} onChange={(valueString) => setAmount(parseFloat(valueString))} min={0.5} step={0.5} precision={2}>
        <NumberInputField />
        <NumberInputStepper>
          <NumberIncrementStepper />
          <NumberDecrementStepper />
        </NumberInputStepper>
      </NumberInput>
      <Text>Minimum 0.50€</Text>
      </Box>
      <Box mb={20}>
      <RadioChoiceBet choiceBet={choiceBet} setChoiceBet={setChoiceBet}/>
      </Box>
    </Box>
  );
};

export default BetModalContent;
