import { Box, Table, Thead, Tbody, Tr, Th, Td, Text } from '@chakra-ui/react';

const QuotationCard = ({match}) => {
    return (
        <Box 
      borderWidth="1px" 
      borderRadius="lg" 
      overflow="hidden" 
      width="100%" 
      bg="white"
      boxShadow="md"
    >
      <Table variant="striped">
        <Thead bg="logo">
          <Tr>
            <Th textAlign="center">Type de Résultat</Th>
            <Th textAlign="center">Cote</Th>
          </Tr>
        </Thead>
        <Tbody>
          <Tr>
            <Td textAlign="center">Victoire équipe domicile</Td>
            <Td textAlign="center">{match.quotation_team_home}</Td>
          </Tr>
          <Tr>
            <Td textAlign="center">Match nul</Td>
            <Td textAlign="center">{match.quotation_draw}</Td>
          </Tr>
          <Tr>
            <Td textAlign="center">Victoire équipe extérieure</Td>
            <Td textAlign="center">{match.quotation_team_ext}</Td>
          </Tr>
        </Tbody>
      </Table>
    </Box>
    );
  };
  
  export default QuotationCard;