import { useEffect, useState } from "react";
import {
  FormLabel,
  FormControl,
  FormErrorMessage,
  Box,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  HStack,
  Text,
} from "@chakra-ui/react";
import SelectTeam from "./SelectTeam";
import SelectWeather from "./SelectWeather";
import DateTimePicker from "./DateTimePicker";
import { IoShirtOutline, IoShirtSharp } from "react-icons/io5";
import { IoMdTime } from "react-icons/io";
import { TiWeatherCloudy } from "react-icons/ti";
import {
  PiPokerChipLight,
  PiPokerChipFill,
  PiPokerChipDuotone,
} from "react-icons/pi";

const AddMatch = ({ newMatch, setNewMatch, errors }) => {

  const handleDateChange = (date) => {
    setNewMatch((prevState) => ({
      ...prevState,
      start_date: date,
    }));
  };

  const handleSetTeamDom = (value) => {
    setNewMatch((prevState) => ({
      ...prevState,
      id_team_home: value,
    }));
  };

  const handleSetTeamExt = (value) => {
    setNewMatch((prevState) => ({
      ...prevState,
      id_team_ext: value,
    }));
  };

  return (
    <>
      <FormControl isInvalid={errors.id_team_home}>
        <FormLabel>
          <HStack>
            <IoShirtOutline />
            <Text>
              Equipe domicile <span className="requiredCustomLabel">*</span>
            </Text>
          </HStack>
        </FormLabel>
        <SelectTeam setTeam={handleSetTeamDom} />
        <FormErrorMessage>Champ requis.</FormErrorMessage>
      </FormControl>

      <FormControl isInvalid={errors.id_team_ext} mt={5}>
        <FormLabel mt={5}>
          <HStack>
            <IoShirtSharp />
            <Text>
              Equipe extérieure <span className="requiredCustomLabel">*</span>
            </Text>
          </HStack>
        </FormLabel>
        <SelectTeam setTeam={handleSetTeamExt} />
        <FormErrorMessage>Champ requis. Ne peut pas être identique a équipe à domicile.</FormErrorMessage>
      </FormControl>

      <FormControl isInvalid={errors.start_date} mt={5}>
        <FormLabel>
          <HStack>
            <IoMdTime />
            <Text>
              Date et heure début du match{" "}
              <span className="requiredCustomLabel">*</span>
            </Text>
          </HStack>
        </FormLabel>
        <DateTimePicker
          label="Selectionner la date et l'heure"
          selectedDate={newMatch.start_date}
          onChange={handleDateChange}
        />
        <FormErrorMessage>Champ requis.</FormErrorMessage>
      </FormControl>

      <FormControl isInvalid={errors.id_weather} mt={5}>
        <FormLabel>
          <HStack>
            <TiWeatherCloudy />
            <Text>
              Météo prévue <span className="requiredCustomLabel">*</span>
            </Text>
          </HStack>
        </FormLabel>
        <SelectWeather setNewMatch={setNewMatch} />
        <FormErrorMessage>Champ requis.</FormErrorMessage>
      </FormControl>

      <FormControl isInvalid={errors.quotation_team_home} mt={5}>
        <FormLabel>
          <HStack>
            <PiPokerChipLight />
            <Text>
              Cote victoire domicile{" "}
              <span className="requiredCustomLabel">*</span>
            </Text>
          </HStack>
        </FormLabel>
        <Box>
          <NumberInput
            defaultValue={1.01}
            min={1.01}
            step={0.01}
            precision={2}
            onChange={(value) =>
              setNewMatch((prevState) => ({
                ...prevState,
                quotation_team_home: value,
              }))
            }
          >
            <NumberInputField />
            <NumberInputStepper>
              <NumberIncrementStepper />
              <NumberDecrementStepper />
            </NumberInputStepper>
          </NumberInput>
        </Box>
        <FormErrorMessage>Champ requis.</FormErrorMessage>
      </FormControl>
      <FormControl isInvalid={errors.quotation_draw} mt={5}>
        <FormLabel mt={5}>
          <HStack>
            <PiPokerChipDuotone />
            <Text>
              Cote match nul <span className="requiredCustomLabel">*</span>
            </Text>
          </HStack>
        </FormLabel>
        <Box>
          <NumberInput
            defaultValue={1.01}
            min={1.01}
            step={0.01}
            precision={2}
            onChange={(value) =>
              setNewMatch((prevState) => ({
                ...prevState,
                quotation_draw: value,
              }))
            }
          >
            <NumberInputField />
            <NumberInputStepper>
              <NumberIncrementStepper />
              <NumberDecrementStepper />
            </NumberInputStepper>
          </NumberInput>
        </Box>
        <FormErrorMessage>Champ requis.</FormErrorMessage>
      </FormControl>
      <FormControl isInvalid={errors.quotation_team_ext} mt={5}>
        <FormLabel mt={5}>
          <HStack>
            <PiPokerChipFill />
            <Text>
              Cote victoire extérieure{" "}
              <span className="requiredCustomLabel">*</span>
            </Text>
          </HStack>
        </FormLabel>
        <Box>
          <NumberInput
            defaultValue={1.01}
            min={1.01}
            step={0.01}
            precision={2}
            onChange={(value) =>
              setNewMatch((prevState) => ({
                ...prevState,
                quotation_team_ext: value,
              }))
            }
          >
            <NumberInputField />
            <NumberInputStepper>
              <NumberIncrementStepper />
              <NumberDecrementStepper />
            </NumberInputStepper>
          </NumberInput>
        </Box>
        <FormErrorMessage>Champ requis.</FormErrorMessage>
      </FormControl>
    </>
  );
};

export default AddMatch;
