import axios from "axios";
import { useState, useEffect } from "react";
import {
  Box,
  Heading,
  Text,
  VStack,
  useToast,
  Spinner,
  Badge,
  Center,
  HStack
} from "@chakra-ui/react";
import { FaRegSurprise } from "react-icons/fa";

const ListMatchOfTheDay = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [hasError, setHasError] = useState(false);
  const toast = useToast();
  const [matchs, setMatchs] = useState("");
  const apiUrl = import.meta.env.VITE_URL_API_SBB_MATCH;

  const getStatus = (status, start_date) => {
    if (status === "A venir") {
      const now = new Date();
      if (now > new Date(start_date)) {
        return "En cours";
      } else {
        return status;
      }
    } else {
      return status;
    }
  };

  const getStatusColor = (status) => {
    if (status === "En cours") {
      return "green";
    } else if (status === "A venir") {
      return "purple";
    } else {
      return "red";
    }
  };

  const getMatchs = async () => {
    setHasError(false);
    setIsLoading(true)
    try {
      const response = await axios.get(
        `${apiUrl}/api/get_all_matchs_of_day`
      );

      setMatchs(response.data["hydra:member"]);
    } catch (error) {
      {
        toast({
          title: "Erreur.",
          description: "Une erreur est survenue.",
          status: "error",
          duration: 9000,
          isClosable: true,
        });
        setHasError(true);
      }
    }
    setIsLoading(false);
  };

  useEffect(() => {
    getMatchs();
  }, []);

  const formatTime = (dateString) => {
    const date = new Date(dateString);
    return date.toLocaleTimeString("fr-FR", {
      hour: "2-digit",
      minute: "2-digit",
    });
  };

  if (isLoading) {
    return (
      <Box display="flex" justifyContent="center" mt={10}>
        <Spinner
          thickness="10px"
          speed="0.80s"
          emptyColor="white"
          color="primary"
          size="xl"
        />
      </Box>
    );
  }

  return (
    <Box>
      <Heading as="h2" mb={4} className="centerText">
        Matchs du jour ({new Date().toLocaleDateString("fr-FR")})
      </Heading>
      {hasError ? (
        <Text className="explication">No data</Text>
      ) : (
        <VStack spacing={4}>
          {matchs.length === 0 && <><Text className="explication">Pas de match aujourd'hui</Text><FaRegSurprise size={30} color="white"/></>}
          {matchs.map((match) => (
            <Box
              key={match.id}
              p={4}
              borderWidth={1}
              borderRadius="md"
              width="100%"
              className={
                getStatus(match.status, match.start_date) !== "En cours"
                  ? "boxDisabled"
                  : "boxActive"
              }
            >
              <Center mb={3}>
                <Badge
                  colorScheme={getStatusColor(
                    getStatus(match.status, match.start_date)
                  )}
                >
                  {getStatus(match.status, match.start_date)}
                </Badge>
              </Center>
              <Text fontFamily={"heading"} textAlign={"center"} fontSize={24} >
                {formatTime(match.start_date).toUpperCase()}
              </Text>
              <HStack justifyContent={"space-around"} mb={3} fontSize={24} fontFamily={"heading"}>
          <Box >
            {match.id_team_home.name} ({match.id_team_home.id_country.code})
          </Box>
          <Box fontFamily={"heading"}>
            <Text>
              Vs
            </Text>
          </Box>
          <Box fontFamily={"heading"}>
            {match.id_team_ext.name} ({match.id_team_ext.id_country.code})
          </Box>
        </HStack>
             
            </Box>
          ))}
        </VStack>
      )}
    </Box>
  );
};

export default ListMatchOfTheDay;
