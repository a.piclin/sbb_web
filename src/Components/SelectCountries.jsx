import { useEffect, useState } from 'react'
import { Select, useToast } from '@chakra-ui/react'
import axios from 'axios';

const SelectCountries = ({setNewCountry}) => {
    const toast = useToast();
    const [listCountries, setListCountries] = useState([]);
    const apiUrl = import.meta.env.VITE_URL_API_SBB_MATCH;

    const getListCountries = async () => {
        try {
          const response = await axios.get(`${apiUrl}/api/get_all_countries`)
          setListCountries(response.data['hydra:member'])
        } catch (error) {
          {
            toast({
              title: 'Erreur.',
              description: 'Une erreur est survenue.',
              status: 'error',
              duration: 9000,
              isClosable: true
            })
          }
        }
      }
    
      useEffect(() => {
        getListCountries()
      }, [])

  return (
    <Select placeholder='Choisir un pays' onChange={(e) => {setNewCountry(e.target.value)} }>
    {listCountries.map((country) => (
          <option value={country['@id']} key={country['@id']}>{country.name}</option>
    ))}
    </Select>
  )
}

export default SelectCountries;