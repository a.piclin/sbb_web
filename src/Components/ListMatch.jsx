import {
  Checkbox,
  HStack,
  VStack,
  Text,
  Center
} from "@chakra-ui/react";
import ListItemMatch from "./ListItemMatch";
import { FaRegSurprise } from "react-icons/fa";

const ListMatch = ({ listMatchs, listSelect = null, setListSelect = null }) => { 

  const changeSelectList = (match) => {
    setListSelect((prevList) => {
      const newSelectedMatches = new Map(prevList);
      if (newSelectedMatches.has(match.id)) {
        newSelectedMatches.delete(match.id);
      } else {
        newSelectedMatches.set(match.id, match);
      }
      return newSelectedMatches;
    });
  };

  if (listSelect === null) {
    return (
      <VStack spacing={4}>
        {listMatchs.length === 0 && <><Text className="explication">Aucun match créé</Text><FaRegSurprise size={30} color="white"/></> }
        {listMatchs.map((match) => (
          <ListItemMatch match={match} />
        ))}
      </VStack>
    );
  }

  return (
    <>
      {listMatchs.length === 0 && <Text className="explication">Pas de match ou aucun match sélectionnable</Text> }
      {listMatchs.map((match) => (
        <HStack key={match.id} mb={5}>
          <Checkbox
            key={match.id}
            isChecked={listSelect.has(match.id)}
            onChange={() => changeSelectList(match)}
          />
          <ListItemMatch match={match} />
        </HStack>
      ))}
    </>
  );
};

export default ListMatch;
