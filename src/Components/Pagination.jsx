import {
  Button,
  HStack,
} from '@chakra-ui/react';

const Pagination = ({ page, setPage, totalPages }) => {
  const handlePrev = () => setPage(page - 1);
  const handleNext = () => setPage(page + 1);
  const handlePageClick = (pageNumber) => setPage(pageNumber);

  const generatePageNumbers = () => {
    const pages = [];
    for (let i = 1; i <= totalPages; i++) {
      if (i === 1 || i === totalPages || (i >= page - 1 && i <= page + 1)) {
        pages.push(i);
      } else if (i === page - 2 || i === page + 2) {
        pages.push('...');
      }
    }
    return pages;
  };

  const pageNumbers = generatePageNumbers();

  return (
    <HStack spacing={2}>
      <Button onClick={handlePrev} isDisabled={page <= 1}>
        &lt;
      </Button>
      {pageNumbers.map((pageNum, index) =>
        pageNum === '...' ? (
          <Button key={index} isDisabled>
            ...
          </Button>
        ) : (
          <Button
            key={index}
            onClick={() => handlePageClick(pageNum)}
            isActive={pageNum === page}
          >
            {pageNum}
          </Button>
        )
      )}
      <Button onClick={handleNext} isDisabled={page >= totalPages}>
        &gt;
      </Button>
    </HStack>
  );
};

export default Pagination;
